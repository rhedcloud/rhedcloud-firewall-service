package edu.emory.it.services.fws.provider;

import com.paloaltonetworks.moa.jmsobjects.firewall.v1_0.FirewallRule;
import com.paloaltonetworks.moa.objects.resources.v1_0.FirewallRuleQuerySpecification;
import org.openeai.config.AppConfig;

import java.util.List;


public interface FirewallRuleProvider {
    /**
     * Initialize the provider.
     * <P>
     * 
     * @param aConfig, an AppConfig object with all this provider needs.
     * <P>
     * @throws ProviderException with details of the initialization error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * Query for the validity of an email address.
     * <P>
     * 
     * @param spec the query spec.
     * @return List<FirewallRule>, the firewall rules for the client.
     * <P>
     * @throws ProviderException with details of the providing the list.
     */
    List<FirewallRule> query(FirewallRuleQuerySpecification spec) throws ProviderException;
}
