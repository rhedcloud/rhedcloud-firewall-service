package edu.emory.it.services.fws.provider;

import com.paloaltonetworks.moa.jmsobjects.firewall.v1_0.FirewallRule;
import com.paloaltonetworks.moa.objects.resources.v1_0.FirewallRuleQuerySpecification;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class ExampleFirewallRuleProvider extends OpenEaiObject implements FirewallRuleProvider {
    private org.apache.logging.log4j.Logger logger = OpenEaiObject.logger;

    private String LOGTAG = "[ExampleFirewallRuleProvider] ";

    private AppConfig appConfig;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        setAppConfig(aConfig);
    }

    public AppConfig getAppConfig() {
        return appConfig;
    }

    public void setAppConfig(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    private void info(String message) {
        logger.info(LOGTAG + " - " + message);
    }

    @Override
    public List<FirewallRule> query(FirewallRuleQuerySpecification spec) throws ProviderException {
        List<FirewallRule> retList = new ArrayList<>();
        if (spec == null) {
            return retList;
        }

        String stringResponse =
                "<response status=\"success\" code=\"19\">\n" +
                        "    <result total-count=\"3\" count=\"3\">\n" +
                        "        <entry name=\"In Admin AWS Testing 1\">\n" +
                        "            <option>\n" +
                        "                <disable-server-response-inspection>no</disable-server-response-inspection>\n" +
                        "            </option>\n" +
                        "            <to>\n" +
                        "                <member>trust</member>\n" +
                        "            </to>\n" +
                        "            <from>\n" +
                        "                <member>untrust</member>\n" +
                        "            </from>\n" +
                        "            <source>\n" +
                        "                <member>aws10.64.63.41</member>\n" +
                        "                <member>AWS10.64.63.85</member>\n" +
                        "                <member>AWS10.64.63.46</member>\n" +
                        "            </source>\n" +
                        "            <destination>\n" +
                        "                <member>intsonicdev1.cc.emory.edu</member>\n" +
                        "            </destination>\n" +
                        "            <source-user>\n" +
                        "                <member>any</member>\n" +
                        "            </source-user>\n" +
                        "            <category>\n" +
                        "                <member>any</member>\n" +
                        "            </category>\n" +
                        "            <application>\n" +
                        "                <member>any</member>\n" +
                        "            </application>\n" +
                        "            <service>\n" +
                        "                <member>tcp-2506</member>\n" +
                        "                <member>tcp-2507</member>\n" +
                        "                <member>tcp-2508</member>\n" +
                        "            </service>\n" +
                        "            <hip-profiles>\n" +
                        "                <member>any</member>\n" +
                        "            </hip-profiles>\n" +
                        "            <action>allow</action>\n" +
                        "            <log-setting>All to Syslog</log-setting>\n" +
                        "            <profile-setting>\n" +
                        "                <group>\n" +
                        "                    <member>Protect-Server</member>\n" +
                        "                </group>\n" +
                        "            </profile-setting>\n" +
                        "            <description>CDT: 17Jan2018 MOD: 01Feb018 OWN: SWHEAT RFTASK65134 JST: Testing AWS Research Service Firewall rules </description>\n" +
                        "            <tag>\n" +
                        "                <member>vpc-5ac8ce22</member>\n" +
                        "                <member>EmoryAWS</member>\n" +
                        "            </tag>\n" +
                        "        </entry>\n" +
                        "        <entry name=\"In Border Admin AWS Testing 1\">\n" +
                        "            <option>\n" +
                        "                <disable-server-response-inspection>no</disable-server-response-inspection>\n" +
                        "            </option>\n" +
                        "            <to>\n" +
                        "                <member>trust</member>\n" +
                        "            </to>\n" +
                        "            <from>\n" +
                        "                <member>untrust</member>\n" +
                        "            </from>\n" +
                        "            <source>\n" +
                        "                <member>aws10.64.63.41</member>\n" +
                        "                <member>AWS10.64.63.85</member>\n" +
                        "                <member>AWS10.64.63.46</member>\n" +
                        "            </source>\n" +
                        "            <destination>\n" +
                        "                <member>intsonicdev1.cc.emory.edu</member>\n" +
                        "            </destination>\n" +
                        "            <source-user>\n" +
                        "                <member>any</member>\n" +
                        "            </source-user>\n" +
                        "            <category>\n" +
                        "                <member>any</member>\n" +
                        "            </category>\n" +
                        "            <application>\n" +
                        "                <member>any</member>\n" +
                        "            </application>\n" +
                        "            <service>\n" +
                        "                <member>tcp-2506</member>\n" +
                        "                <member>tcp-2507</member>\n" +
                        "                <member>tcp-2508</member>\n" +
                        "            </service>\n" +
                        "            <hip-profiles>\n" +
                        "                <member>any</member>\n" +
                        "            </hip-profiles>\n" +
                        "            <action>allow</action>\n" +
                        "            <log-setting>All to Syslog</log-setting>\n" +
                        "            <profile-setting>\n" +
                        "                <group>\n" +
                        "                    <member>Protect-Server</member>\n" +
                        "                </group>\n" +
                        "            </profile-setting>\n" +
                        "            <description>CDT: 17Jan2018 MOD: 01Feb2018 OWN: SWHEAT RFTASK65134 JST: Testing AWS Research Service Firewall rules </description>\n" +
                        "            <tag>\n" +
                        "                <member>vpc-5ac8ce22</member>\n" +
                        "                <member>Admin</member>\n" +
                        "            </tag>\n" +
                        "            <disabled>yes</disabled>\n" +
                        "        </entry>\n" +
                        "        <entry name=\"In AWS Web Traffic Allow\">\n" +
                        "            <profile-setting>\n" +
                        "                <group>\n" +
                        "                    <member>Alert-Only</member>\n" +
                        "                </group>\n" +
                        "            </profile-setting>\n" +
                        "            <to>\n" +
                        "                <member>trust</member>\n" +
                        "            </to>\n" +
                        "            <from>\n" +
                        "                <member>untrust</member>\n" +
                        "            </from>\n" +
                        "            <source>\n" +
                        "                <member>any</member>\n" +
                        "            </source>\n" +
                        "            <destination>\n" +
                        "                <member>AWS10.64.63.52</member>\n" +
                        "                <member>AWS10.64.63.85</member>\n" +
                        "            </destination>\n" +
                        "            <source-user>\n" +
                        "                <member>any</member>\n" +
                        "            </source-user>\n" +
                        "            <category>\n" +
                        "                <member>any</member>\n" +
                        "            </category>\n" +
                        "            <application>\n" +
                        "                <member>any</member>\n" +
                        "            </application>\n" +
                        "            <service>\n" +
                        "                <member>HTTP</member>\n" +
                        "            </service>\n" +
                        "            <hip-profiles>\n" +
                        "                <member>any</member>\n" +
                        "            </hip-profiles>\n" +
                        "            <action>allow</action>\n" +
                        "            <description>CDT: 17Jan2018 MOD: 17Jan2018 OWN: SWHEAT RFTASK64516 JST: Testing AWS inbound NAT Web</description>\n" +
                        "            <log-setting>All to Syslog</log-setting>\n" +
                        "            <tag>\n" +
                        "                <member>vpc-5ac8ce22</member>\n" +
                        "            </tag>\n" +
                        "        </entry>\n" +
                        "    </result>\n" +
                        "</response>";

        // Get the root element from the document returned.
        Element root = null;


        try {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(new StringReader(stringResponse));
            root = doc.getRootElement();
            String rootElementName = root.getName();
            info(LOGTAG + " Root element name: " + rootElementName);
            info(LOGTAG + "There are " + root.getChild("result").getChildren().size() +
                    " result elements in the document returned.");

            for (int i = 0; i < root.getChild("result").getChildren().size(); i++) {
                Element element = (Element) root.getChild("result").getChildren().get(i);
                try {
                    FirewallRule firewallRule = (FirewallRule) getAppConfig().getObject("FirewallRule.v1_0");

                    // Set the current layout manager to be the ATTVlanXmlLayout layout manager.
                    EnterpriseLayoutManager elm = (EnterpriseLayoutManager) firewallRule
                            .getInputLayoutManagers().get("other");
                    firewallRule.setInputLayoutManager(elm);

                    firewallRule.buildObjectFromInput(element);

                    if (firewallRule.getName() != null) {
                        // Set the layout manager back to the XML layout manager.
                        elm = (EnterpriseLayoutManager) firewallRule.getInputLayoutManagers()
                                .get("xml");
                        firewallRule.setInputLayoutManager(elm);
                        retList.add(firewallRule);
                    }
                } catch (EnterpriseConfigurationObjectException e) {
                    e.printStackTrace();
                    String errMsg = "Error parsing response from PaloAltoNetworks API response (1).  "
                            + "The exception is: " + e.getMessage();
                    logger.error(LOGTAG + errMsg);
                    throw new ProviderException(e.getMessage(), e);
                } catch (EnterpriseLayoutException e) {
                    e.printStackTrace();
                    String errMsg = "Error parsing response from PaloAltoNetworks API response (2).  "
                            + "The exception is: " + e.getMessage();
                    logger.error(LOGTAG + errMsg);
                    throw new ProviderException(e.getMessage(), e);
                }
            }

        } catch (JDOMException e) {
            e.printStackTrace();
            String errMsg = "Error parsing response from PaloAltoNetworks API (3).  "
                    + "The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg, e);
        } catch (IOException e) {
            e.printStackTrace();
            String errMsg = "Error parsing response from PaloAltoNetworks (4).  "
                    + "The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg, e);
        }

        return retList;
    }

}
