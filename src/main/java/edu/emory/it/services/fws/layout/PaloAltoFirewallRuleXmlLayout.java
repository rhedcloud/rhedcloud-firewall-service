package edu.emory.it.services.fws.layout;

import com.paloaltonetworks.moa.jmsobjects.firewall.v1_0.FirewallRule;
import com.paloaltonetworks.moa.objects.resources.v1_0.Application;
import com.paloaltonetworks.moa.objects.resources.v1_0.Category;
import com.paloaltonetworks.moa.objects.resources.v1_0.Destination;
import com.paloaltonetworks.moa.objects.resources.v1_0.From;
import com.paloaltonetworks.moa.objects.resources.v1_0.Group;
import com.paloaltonetworks.moa.objects.resources.v1_0.HipProfiles;
import com.paloaltonetworks.moa.objects.resources.v1_0.ProfileSetting;
import com.paloaltonetworks.moa.objects.resources.v1_0.Service;
import com.paloaltonetworks.moa.objects.resources.v1_0.Source;
import com.paloaltonetworks.moa.objects.resources.v1_0.SourceUser;
import com.paloaltonetworks.moa.objects.resources.v1_0.Tag;
import com.paloaltonetworks.moa.objects.resources.v1_0.To;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;
import org.openeai.layouts.EnterpriseLayoutManagerImpl;
import org.openeai.moa.XmlEnterpriseObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by hen-surge on 2018-02-06.
 */
public class PaloAltoFirewallRuleXmlLayout extends EnterpriseLayoutManagerImpl implements EnterpriseLayoutManager, Serializable {
    private static Logger logger = LogManager
            .getLogger("edu.emory.it.services.fws");

    /**
     * Empty constructor
     */
    public PaloAltoFirewallRuleXmlLayout() {

    }

    public void init(String layoutManagerName, Document layoutDoc) throws EnterpriseLayoutException {
        super.init(layoutManagerName, layoutDoc);
    }

    @Override
    public void buildObjectFromInput(Object inputObj, XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
		/* builds a single VLAN MOA object from an ATT provided vlan JDOM element
		 * The JDOM element was created in the provider (ATTVlanProvider) but we could do that here as well
		 *
		 */
        Element eEntry = (Element)inputObj;
        FirewallRule firewallRule = (FirewallRule)xeo;

        if (eEntry.getChild("disabled") != null &&
        	eEntry.getChild("disabled").getText().equals("yes")) {

            return;
        }

        try {
            firewallRule.setName(eEntry.getAttributeValue("name"));

            if (eEntry.getChild("from") != null) {
                List eleFrom = eEntry.getChild("from").getChildren();
                From from = new From();
                firewallRule.setFrom(from);
                for (int i = 0; i < eleFrom.size(); i++) {
                    Element member = (Element) eleFrom.get(i);
                    from.addMember(member.getText());
                }
            }

            if (eEntry.getChild("to") != null) {
                List eleTo = eEntry.getChild("to").getChildren();
                To to = new To();
                firewallRule.setTo(to);
                for (int i = 0; i < eleTo.size(); i++) {
                    Element member = (Element) eleTo.get(i);
                    to.addMember(member.getText());
                }
            }

            if (eEntry.getChild("source") != null) {
                List eleSource = eEntry.getChild("source").getChildren();
                Source source = new Source();
                firewallRule.setSource(source);
                for (int i = 0; i < eleSource.size(); i++) {
                    Element member = (Element) eleSource.get(i);
                    source.addMember(member.getText());
                }
            }

            if (eEntry.getChild("destination") != null) {
                List eleDest = eEntry.getChild("destination").getChildren();
                Destination destination = new Destination();
                firewallRule.setDestination(destination);
                for (int i = 0; i < eleDest.size(); i++) {
                    Element member = (Element) eleDest.get(i);
                    destination.addMember(member.getText());
                }
            }

            if (eEntry.getChild("service") != null) {
                List eleService = eEntry.getChild("service").getChildren();
                Service service = new Service();
                firewallRule.setService(service);
                for (int i = 0; i < eleService.size(); i++) {
                    Element member = (Element) eleService.get(i);
                    service.addMember(member.getText());
                }
            }

            if (eEntry.getChild("application") != null) {
                List eleApplication = eEntry.getChild("application").getChildren();
                Application application = new Application();
                firewallRule.setApplication(application);
                for (int i = 0; i < eleApplication.size(); i++) {
                    Element member = (Element) eleApplication.get(i);
                    application.addMember(member.getText());
                }
            }

            if (eEntry.getChild("action") != null) {
                firewallRule.setAction(eEntry.getChild("action").getText());
            }

            if (eEntry.getChild("tag") != null) {
                List eleTag = eEntry.getChild("tag").getChildren();
                Tag tag = new Tag();
                firewallRule.setTag(tag);
                for (int i = 0; i < eleTag.size(); i++) {
                    Element member = (Element) eleTag.get(i);
                    tag.addMember(member.getText());
                }
            }

            if (eEntry.getChild("source-user") != null) {
                List eleSourceUser = eEntry.getChild("source-user").getChildren();
                SourceUser sourceUser = new SourceUser();
                firewallRule.setSourceUser(sourceUser);
                for (int i = 0; i < eleSourceUser.size(); i++) {
                    Element member = (Element) eleSourceUser.get(i);
                    sourceUser.addMember(member.getText());
                }
            }

            if (eEntry.getChild("hip-profiles") != null) {
                List eleHipProfiles = eEntry.getChild("hip-profiles").getChildren();
                HipProfiles hipProfiles = new HipProfiles();
                firewallRule.setHipProfiles(hipProfiles);
                for (int i = 0; i < eleHipProfiles.size(); i++) {
                    Element member = (Element) eleHipProfiles.get(i);
                    hipProfiles.addMember(member.getText());
                }
            }

            if (eEntry.getChild("profile-setting") != null &&
            	eEntry.getChild("profile-setting").getChild("group") != null) {

                List eleGroup = eEntry.getChild("profile-setting").getChild("group").getChildren();
                ProfileSetting profileSetting = new ProfileSetting();
                Group group = profileSetting.newGroup();
                profileSetting.setGroup(group);
                firewallRule.setProfileSetting(profileSetting);
                for (int i = 0; i < eleGroup.size(); i++) {
                    Element member = (Element) eleGroup.get(i);
                    group.addMember(member.getText());
                }
            }

            if (eEntry.getChild("category") != null) {
                List eleCategory = eEntry.getChild("category").getChildren();
                Category category = new Category();
                firewallRule.setCategory(category);
                for (int i = 0; i < eleCategory.size(); i++) {
                    Element member = (Element) eleCategory.get(i);
                    category.addMember(member.getText());
                }
            }

            if (eEntry.getChild("description") != null) {
                firewallRule.setDescription(eEntry.getChild("description").getText());
            }

            if (eEntry.getChild("log-setting") != null) {
                firewallRule.setLogSetting(eEntry.getChild("log-setting").getText());
            }

        } catch (EnterpriseFieldException e) {
            e.printStackTrace();
            String errMsg = "Error populating FirewallRule object from element returned.  "
                    + "The exception is: " + e.getMessage();
            throw new EnterpriseLayoutException(errMsg, e);
        }

		/*
		<entry name="In Academic AWS AD access">
            <from>
                <member>untrust</member>
            </from>
            <to>
                <member>trust</member>
            </to>
            <source>
                <member>AWSVPCs10.64.0.0-16</member>
            </source>
            <destination>
                <member>ADserversAcad</member>
            </destination>
            <service>
                <member>any</member>
            </service>
            <application>
                <member>Microsoft-Base</member>
            </application>
            <action>allow</action>
            <log-end>yes</log-end>
            <negate-source>no</negate-source>
            <negate-destination>no</negate-destination>
            <option>
                <disable-server-response-inspection>no</disable-server-response-inspection>
            </option>
            <tag>
                <member>EmoryAWS</member>
            </tag>
            <source-user>
                <member>any</member>
            </source-user>
            <hip-profiles>
                <member>any</member>
            </hip-profiles>
            <log-start>no</log-start>
            <profile-setting>
                <group>
                    <member>Alert-Only</member>
                </group>
            </profile-setting>
            <category>
                <member>any</member>
            </category>
            <description>CDT: 02Feb2018 MOD: 02FEB2018 OWN: AEFTING </description>
            <log-setting>All to Syslog</log-setting>
            <disabled>no</disabled>
        </entry>
		 */
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo) throws EnterpriseLayoutException {

        FirewallRule firewallRule = (FirewallRule)xeo;
        Element firewallRuleElm = new Element("FirewallRule");

        firewallRuleElm.addContent(new Element("Name").setText(firewallRule.getName()));

        if (firewallRule.getFrom() != null) {
            Element from = new Element("From");
            firewallRuleElm.addContent(from);
            for (Object member : firewallRule.getFrom().getMember()) {
                from.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getTo() != null) {
            Element to = new Element("To");
            firewallRuleElm.addContent(to);
            for (Object member : firewallRule.getTo().getMember()) {
                to.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getSource() != null) {
            Element source = new Element("Source");
            firewallRuleElm.addContent(source);
            for (Object member : firewallRule.getSource().getMember()) {
                source.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getDestination() != null) {
            Element destination = new Element("Destination");
            firewallRuleElm.addContent(destination);
            for (Object member : firewallRule.getDestination().getMember()) {
                destination.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getService() != null) {
            Element service = new Element("Service");
            firewallRuleElm.addContent(service);
            for (Object member : firewallRule.getService().getMember()) {
                service.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getApplication() != null) {
            Element application = new Element("Application");
            firewallRuleElm.addContent(application);
            for (Object member : firewallRule.getApplication().getMember()) {
                application.addContent(new Element("Member").setText((String) member));
            }
        }

        firewallRuleElm.addContent(new Element("Action").setText(firewallRule.getAction()));

        if (firewallRule.getTag() != null) {
            Element tag = new Element("Tag");
            firewallRuleElm.addContent(tag);
            for (Object member : firewallRule.getTag().getMember()) {
                tag.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getSourceUser() != null) {
            Element sourceUser = new Element("SourceUser");
            firewallRuleElm.addContent(sourceUser);
            for (Object member : firewallRule.getSourceUser().getMember()) {
                sourceUser.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getHipProfiles() != null) {
            Element hipProfiles = new Element("HipProfiles");
            firewallRuleElm.addContent(hipProfiles);
            for (Object member : firewallRule.getHipProfiles().getMember()) {
                hipProfiles.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getProfileSetting() != null && firewallRule.getProfileSetting().getGroup() != null) {
            Element profileSettings = new Element("ProfileSetting");
            firewallRuleElm.addContent(profileSettings);
            Element group = new Element("Group");
            profileSettings.addContent(group);
            for (Object member : firewallRule.getProfileSetting().getGroup().getMember()) {
                group.addContent(new Element("Member").setText((String) member));
            }
        }

        if (firewallRule.getCategory() != null) {
            Element category = new Element("Category");
            firewallRuleElm.addContent(category);
            for (Object member : firewallRule.getCategory().getMember()) {
                category.addContent(new Element("Member").setText((String) member));
            }
        }

        firewallRuleElm.addContent(new Element("Description").setText(firewallRule.getDescription()));

        firewallRuleElm.addContent(new Element("LogSetting").setText(firewallRule.getLogSetting()));

        /*
		<?xml version="1.0" encoding="UTF-8" ?>
		<FirewallRule>
			<Name>In AWS Web Traffic Allow</Name>
			<ProfileSetting>
				<Group>
					<Member>Alert-Only</Member>
				</Group>
			</ProfileSetting>
			<To>
				<Member>trust</Member>
			</To>
			<From>
				<Member>untrust</Member>
			</From>
			<Source>
				<Member>any</Member>
			</Source>
			<Destination>
				<Member>AWS10.64.63.52</Member>
				<Member>AWS10.64.63.85</Member>
			</Destination>
			<SourceUser>
				<Member>any</Member>
			</SourceUser>
			<Category>
				<Member>any</Member>
			</Category>
			<Application>
				<Member>any</Member>
			</Application>
			<Service>
				<Member>HTTP</Member>
			</Service>
			<HipProfiles>
				<Member>any</Member>
			</HipProfiles>
			<Action>allow</Action>
			<Description>CDT: 17Jan2018 MOD: 17Jan2018 OWN: SWHEAT RFTASK64516 JST: Testing AWS
				inbound NAT Web</Description>
			<LogSetting>All to Syslog</LogSetting>
			<Tag>
				<Member>vpc-5ac8ce22</Member>
			</Tag>
		</FirewallRule>
		 */

        return firewallRuleElm;
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo, String appName) throws EnterpriseLayoutException {
        String LOGTAG = "[PaloAltoFirewallRuleXmlLayout.buildOutputFromObject] ";

        logger.trace(LOGTAG
                + "Build Object From Input with AppName - Started Processing.");
        setTargetAppName(appName);

        Object obj = buildOutputFromObject(xeo);

        logger.trace(LOGTAG
                + "Build Object From Input with AppName - Ended Processing.");

        return obj;
    }

}
