package edu.emory.it.services.fws.provider;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;
import org.openeai.moa.XmlEnterpriseObjectException;

import com.paloaltonetworks.moa.jmsobjects.firewall.v1_0.FirewallRule;
import com.paloaltonetworks.moa.objects.resources.v1_0.FirewallRuleQuerySpecification;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class PanoramaFirewallRuleProvider extends OpenEaiObject implements FirewallRuleProvider {

    private Logger logger = OpenEaiObject.logger;
    private static final String PANORAMA_FIREWALL_RULE_PROVIDER_PROPERTIES = "PanoramaFirewallRuleProvider";

    private String LOGTAG = "[PanoramaFirewallRuleProvider] ";
    private AppConfig appConfig;

    private Client serviceClient;

    private String baseServiceUrl;
    private String apiKey;
    private String rulesXPath;
    private boolean verbose=false;


    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        info("Initializing");
        appConfig = aConfig;

        try {
            setProperties(aConfig.getProperties(PANORAMA_FIREWALL_RULE_PROVIDER_PROPERTIES));
        } catch (EnterpriseConfigurationObjectException e) {
            e.printStackTrace();
            String errMsg = "Error retrieving a PropertyConfig object from "
                    + "AppConfig: The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg, e);
        }

        ClientConfig cc = new DefaultClientConfig();
        cc.getProperties().put(
                ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
        Client client = Client.create(cc);
        client.setConnectTimeout(20*1000);
        client.setReadTimeout(30*1000);  //5 minutes
        client.setFollowRedirects(true);

        baseServiceUrl = getProperties().getProperty("baseServiceUrl");
        apiKey = getProperties().getProperty("apiKey");
        rulesXPath = getProperties().getProperty("rulesXPath");
        verbose = Boolean.parseBoolean(getProperties().getProperty("verbose", "false"));
        info("verbose=" + verbose);

        // Set the service client for this provider to use.
        serviceClient = client;
    }

    private void info(String message) {
        logger.info(LOGTAG + " - " + message);
    }


    @SuppressWarnings("unchecked")
	@Override
    public List<FirewallRule> query(FirewallRuleQuerySpecification spec) throws ProviderException {
    	long queryStartTime = System.currentTimeMillis();
        List<FirewallRule> retList = new ArrayList<>();
        if (spec == null ||
       		spec.getTag() == null ||
       		spec.getTag().getMember() == null ||
       		spec.getTag().getMember().isEmpty()) {

            info(LOGTAG + "returning empty object because input is missing "
            		+ "FirewallRuleQuerySpecification or missing members in "
            		+ "the tag element");
            return retList;
        }


        info(LOGTAG + "using serviceUrl:  " + baseServiceUrl + " For vpcId: "
        	+ spec.getTag().getMember().stream().collect(Collectors.joining(",")));

        String ruleNames = (String) spec.getTag().getMember().stream().map( member -> ".=\"" + member + "\"" ).collect(Collectors.joining(" or "));
        info(LOGTAG + "ruleNames is: '" + ruleNames);

    	long parseSpecStartTime = System.currentTimeMillis();
        List<String> tags = new java.util.ArrayList<String>();
        for (String member : (List<String>)spec.getTag().getMember()) {
        	tags.add(member);
        }
    	long parseSpecEndTime = System.currentTimeMillis();
    	long retrieveRulesStartTime = System.currentTimeMillis();
    	retList = retrieveSecurityRules(rulesXPath, tags);
    	long retrieveRulesEndTime = System.currentTimeMillis();
    	long queryEndTime = System.currentTimeMillis();

    	info(LOGTAG + "query time: " + (queryEndTime - queryStartTime));
    	info(LOGTAG + "parse spec time: " + (parseSpecEndTime - parseSpecStartTime));
    	info(LOGTAG + "retrieve rules time: " + (retrieveRulesEndTime - retrieveRulesStartTime));

        return retList;
    }

    private List<FirewallRule> retrieveSecurityRules(String rulesXPath, List<String> tags) throws ProviderException {
    	info(LOGTAG + "looking for firewall rules that have the following tags: " + tags);
        List<FirewallRule> firewallRules = new ArrayList<>();
    	Element root = retrieveConfig(rulesXPath);

    	for (int i=0; i<root.getChild("result").getChild("device-group").getChildren().size(); i++) {
    		Element eDeviceGroupEntry = (Element) root.getChild("result").getChild("device-group").getChildren().get(i);
    		String deviceGroupName = eDeviceGroupEntry.getAttributeValue("name");

    		int ruleSize=0;
    		Element ePostRuleBase = eDeviceGroupEntry.getChild("post-rulebase");
    		if (ePostRuleBase != null) {
    			Element eSecurity = ePostRuleBase.getChild("security");
    			if (eSecurity != null) {
    				Element eRules = eSecurity.getChild("rules");
    				if (eRules != null) {
        				ruleSize = eRules.getChildren().size();
    				}
    				else {
    					info("No child 'rules' for 'security' element, continuing to next entry");
    					continue;
    				}
    			}
    			else {
					info("No child 'security' for 'post-rulebase' element, continuing to next entry");
    				continue;
    			}
    		}
    		else {
				info("No 'post-rulebase' child for 'device-group' named " +
					deviceGroupName + " element, continuing to next entry");
    			continue;
    		}

//    		for (int j=0; j<eDeviceGroupEntry.getChild("post-rulebase").getChild("security").getChild("rules").getChildren().size(); j++) {
    		for (int j=0; j<ruleSize; j++) {
            	Element eEntry = (Element) eDeviceGroupEntry.getChild("post-rulebase").getChild("security").getChild("rules").getChildren().get(j);
                String ruleName = eEntry.getAttributeValue("name");
                if (verbose) {
                    info(LOGTAG + "entryname:  " + ruleName);
                }

                // build a list of FirewallRule objects from each entry
                // that has a matching entry/tag with the vpc id we're looking for
                for (String tag : tags) {
                	boolean foundIt = false;
                	Element eTag = eEntry.getChild("tag");
                	if (eTag != null) {
                    	memberLoop: for (int k=0; k<eTag.getChildren().size(); k++) {
                    		Element eMember = (Element) eTag.getChildren().get(k);
                    		String sMember = eMember.getText();
                    		if (sMember != null && sMember.equalsIgnoreCase(tag)) {
                    			foundIt = true;
                    			break memberLoop;
                    		}
                    	}
                	}
                	if (foundIt) {
            			info("BUILD A FIREWALL RULE FOR TAG/MEMBER: " + tag + " FOR RULE: " + ruleName);
                        try {
                            FirewallRule firewallRule = (FirewallRule) appConfig.getObject("FirewallRule.v1_0");

                            EnterpriseLayoutManager elm = (EnterpriseLayoutManager) firewallRule
                                    .getInputLayoutManagers().get("other");
                            firewallRule.setInputLayoutManager(elm);

                            firewallRule.buildObjectFromInput(eEntry);
                            firewallRule.setDeviceGroupName(deviceGroupName);

                            // Set the layout manager back to the XML layout manager.
                            elm = (EnterpriseLayoutManager) firewallRule.getInputLayoutManagers()
                                    .get("xml");
                            firewallRule.setInputLayoutManager(elm);

                            if (verbose) {
                                info(LOGTAG + " adding FirewallRule " + firewallRule.toXmlString());
                            }
                            firewallRules.add(firewallRule);
                        } catch (EnterpriseConfigurationObjectException e) {
                            e.printStackTrace();
                            String errMsg = "Error parsing response from PaloAltoNetworks API response (1).  "
                                    + "The exception is: " + e.getMessage();
                            logger.error(LOGTAG + errMsg);
                            throw new ProviderException(e.getMessage(), e);
                        } catch (EnterpriseLayoutException e) {
                            e.printStackTrace();
                            String errMsg = "Error parsing response from PaloAltoNetworks API response (2).  "
                                    + "The exception is: " + e.getMessage();
                            logger.error(LOGTAG + errMsg);
                            throw new ProviderException(e.getMessage(), e);
                        }
						catch (XmlEnterpriseObjectException e) {
                            e.printStackTrace();
                            String errMsg = "Error serializeing object to XML (3).  "
                                    + "The exception is: " + e.getMessage();
                            logger.error(LOGTAG + errMsg);
                            throw new ProviderException(e.getMessage(), e);
						}
						catch (EnterpriseFieldException e) {
							e.printStackTrace();
                            String errMsg = "Error setting device group name (4).  "
                                    + "The exception is: " + e.getMessage();
                            logger.error(LOGTAG + errMsg);
                            throw new ProviderException(e.getMessage(), e);
						}
                	}
                	else {
//                    		info("COULD NOT FIND A MATCHING FIREWALL RULE FOR TAG/MEMBER: " + tag);
                	}
//                    }
                }
    		}
    	}

    	info(LOGTAG + "returning " + firewallRules.size() + " firewall rules "
   			+ "to the FirewallRuleCommand");
    	return firewallRules;
    }

    private Element retrieveConfig(String xpath) throws ProviderException {
        WebResource service = serviceClient
                .resource(baseServiceUrl)
                .queryParam("type", "config")
                .queryParam("action", "get")
                .queryParam("xpath", xpath)
                .queryParam("key", apiKey);

        long startTime = System.currentTimeMillis();
        ClientResponse response = service
                .type(MediaType.APPLICATION_XML_TYPE)
                .get(ClientResponse.class);

        info(LOGTAG + "Request as a string:  " + service.toString());
        long time = System.currentTimeMillis() - startTime;
        info(LOGTAG + "Response received in: " + time + " ms.");
        info(LOGTAG + "Response from server: " + response.toString());
        info(LOGTAG + "Response headers:     " + response.getHeaders());
        String stringResponse = response.getEntity(String.class);
        if (response.getStatus() != 200) {
            String errMsg = "ERROR:  API GET request failed. "
                    + response.toString();
            info(LOGTAG + errMsg);
            // throw an exception with the error info and let the command build the response
            throw new ProviderException(errMsg);
        }
        else {
        	info(LOGTAG + "API GET request was successfull");
        	if (verbose) {
                info(LOGTAG + "query results: " + stringResponse);
        	}

            // Get the root element from the document returned.
            Element root = null;

            try {
                SAXBuilder builder = new SAXBuilder();
                Document doc = builder.build(new StringReader(stringResponse));
                root = doc.getRootElement();
                String rootElementName = root.getName();
                info(LOGTAG + " Root element name: " + rootElementName);
                info(LOGTAG + "There are " + root.getChild("result").getChildren().size() +
                        " result elements in the document returned.");

                return root;
            }
            catch (JDOMException e) {
                e.printStackTrace();
                String errMsg = "Error parsing response from PaloAltoNetworks API (3).  "
                        + "The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new ProviderException(errMsg, e);
            }
            catch (IOException e) {
                e.printStackTrace();
                String errMsg = "Error parsing response from PaloAltoNetworks (4).  "
                        + "The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new ProviderException(errMsg, e);
            }
        }
    }
}
