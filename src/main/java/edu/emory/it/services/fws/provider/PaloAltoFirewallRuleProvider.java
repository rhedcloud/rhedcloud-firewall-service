package edu.emory.it.services.fws.provider;

import com.paloaltonetworks.moa.jmsobjects.firewall.v1_0.FirewallRule;
import com.paloaltonetworks.moa.objects.resources.v1_0.FirewallRuleQuerySpecification;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PaloAltoFirewallRuleProvider extends OpenEaiObject implements FirewallRuleProvider {

    private Logger logger = OpenEaiObject.logger;
    private static final String PALO_ALTO_FIREWALL_RULE_PROVIDER_PROPERTIES = "PaloAltoFirewallRuleProvider";

    private String LOGTAG = "[FirewallRuleProvider] ";
    private AppConfig appConfig;

    private Client serviceClient;

    private String baseServiceUrl;
    private String authHeader;
    private String vsysNameXPath;
    private String vsysDisplayNameXPath;
    private String vsysRulesXPath;


    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        info("Initializing");
        appConfig = aConfig;

        try {
            setProperties(aConfig.getProperties(PALO_ALTO_FIREWALL_RULE_PROVIDER_PROPERTIES));
        } catch (EnterpriseConfigurationObjectException e) {
            e.printStackTrace();
            String errMsg = "Error retrieving a PropertyConfig object from "
                    + "AppConfig: The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg, e);
        }

        ClientConfig cc = new DefaultClientConfig();
        cc.getProperties().put(
                ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
        Client client = Client.create(cc);
        client.setConnectTimeout(20*1000);
        client.setReadTimeout(30*1000);  //5 minutes
        client.setFollowRedirects(true);

        baseServiceUrl = getProperties().getProperty("baseServiceUrl");
        authHeader = getProperties().getProperty("authHeader");
        vsysNameXPath = getProperties().getProperty("vsysNameXPath");
        vsysDisplayNameXPath = getProperties().getProperty("vsysDisplayNameXPath");
        vsysRulesXPath = getProperties().getProperty("vsysRulesXPath");

        // Set the service client for this provider to use.
        serviceClient = client;
    }

    private void info(String message) {
        logger.info(LOGTAG + " - " + message);
    }


    @Override
    public List<FirewallRule> query(FirewallRuleQuerySpecification spec) throws ProviderException {
        List<FirewallRule> retList = new ArrayList<>();
        if (spec == null ||
       		spec.getTag() == null ||
       		spec.getTag().getMember() == null ||
       		spec.getTag().getMember().isEmpty()) {

            info(LOGTAG + "returning empty object because input is missing "
            		+ "FirewallRuleQuerySpecification or missing members in "
            		+ "the tag element");
            return retList;
        }


        info(LOGTAG + "using serviceUrl:  " + baseServiceUrl + " For vpcId: "
        	+ spec.getTag().getMember().stream().collect(Collectors.joining(",")));

        String ruleNames = (String) spec.getTag().getMember().stream().map( member -> ".=\"" + member + "\"" ).collect(Collectors.joining(" or "));
        List<String> vsysIds = retrieveVsysIds(ruleNames);
        List<String> vsysNames = retrieveVsysNames(vsysIds);

        for (int i = 0; i < vsysIds.size(); i++) {
            retList.addAll(retrieveVsysRules(vsysIds.get(i), vsysNames.get(i), ruleNames));
        }

        return retList;
    }


    private List<String> retrieveVsysIds(String tag) throws ProviderException {
        Element root = retrieveConfig(String.format(vsysNameXPath, tag));
        List<String> ids = new ArrayList<>();

        for (int i=0; i<root.getChild("result").getChildren().size(); i++) {
            Element element = (Element) root.getChild("result").getChildren().get(i);
            ids.add(element.getAttributeValue("name"));
        }

        return ids;
    }

    private List<String> retrieveVsysNames(List<String> vsysIds) throws ProviderException {
        List<String> names = new ArrayList<>();
        String nameAttr = vsysIds.stream().map( id -> "@name='" + id + "'").collect(Collectors.joining(" or "));
        Element root = retrieveConfig(String.format(vsysDisplayNameXPath, nameAttr));

        for (int i=0; i<root.getChild("result").getChildren().size(); i++) {
            Element element = (Element) root.getChild("result").getChildren().get(i);
            names.add(element.getValue());
        }

        if (vsysIds.size() != names.size()) {
            String errMsg = "Retrieved vsys names is a different size than the number of vsys ids";
            logger.error(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        return names;
    }

    private List<FirewallRule> retrieveVsysRules(String vsysId, String vsysName, String ruleNames) throws ProviderException {
        Element root = retrieveConfig(String.format(vsysRulesXPath, vsysId, ruleNames));
        List<FirewallRule> firewallRules = new ArrayList<>();

        for (int i=0; i<root.getChild("result").getChildren().size(); i++) {
            Element element = (Element) root.getChild("result").getChildren().get(i);
            try {
                FirewallRule firewallRule = (FirewallRule) appConfig.getObject("FirewallRule.v1_0");

                EnterpriseLayoutManager elm = (EnterpriseLayoutManager) firewallRule
                        .getInputLayoutManagers().get("other");
                firewallRule.setInputLayoutManager(elm);

                firewallRule.buildObjectFromInput(element);

                if (firewallRule.getName() != null) {

                    String vsys = vsysId + " - " + vsysName;

                    // set vsys on FirewallRule MOA object
                    firewallRule.setVsys(vsys);

                    // Set the layout manager back to the XML layout manager.
                    elm = (EnterpriseLayoutManager) firewallRule.getInputLayoutManagers()
                            .get("xml");
                    firewallRule.setInputLayoutManager(elm);

                    firewallRules.add(firewallRule);
                }
            } catch (EnterpriseConfigurationObjectException e) {
                e.printStackTrace();
                String errMsg = "Error parsing response from PaloAltoNetworks API response (1).  "
                        + "The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new ProviderException(e.getMessage(), e);
            } catch (EnterpriseLayoutException e) {
                e.printStackTrace();
                String errMsg = "Error parsing response from PaloAltoNetworks API response (2).  "
                        + "The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new ProviderException(e.getMessage(), e);
            } catch (EnterpriseFieldException e) {
                e.printStackTrace();
                String errMsg = "Error setting VSYS field.  "
                        + "The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new ProviderException(e.getMessage(), e);
			}
        }

        return firewallRules;
    }

    private Element retrieveConfig(String xpath) throws ProviderException {
        WebResource service = serviceClient
                .resource(baseServiceUrl)
                .queryParam("type", "config")
                .queryParam("action", "get")
                .queryParam("xpath", xpath);

        long startTime = System.currentTimeMillis();
        ClientResponse response = service
                .header("Authorization", "Basic " + authHeader)
                .type(MediaType.APPLICATION_XML_TYPE)
                .get(ClientResponse.class);

        info(LOGTAG + "Request as a string:  " + service.toString());
        long time = System.currentTimeMillis() - startTime;
        info(LOGTAG + "Response received in: " + time + " ms.");
        info(LOGTAG + "Response from server: " + response.toString());
        info(LOGTAG + "Response headers:     " + response.getHeaders());
        String stringResponse = response.getEntity(String.class);
        if (response.getStatus() != 200) {
            String errMsg = "ERROR:  API GET request failed. "
                    + response.toString();
            info(LOGTAG + errMsg);
            // throw an exception with the error info and let the command build the response
            throw new ProviderException(errMsg);
        }
        else {
            info(LOGTAG + "Generate/create results: " + stringResponse);

            // Get the root element from the document returned.
            Element root = null;

            try {
                SAXBuilder builder = new SAXBuilder();
                Document doc = builder.build(new StringReader(stringResponse));
                root = doc.getRootElement();
                String rootElementName = root.getName();
                info(LOGTAG + " Root element name: " + rootElementName);
                info(LOGTAG + "There are " + root.getChild("result").getChildren().size() +
                        " result elements in the document returned.");

                return root;
            }
            catch (JDOMException e) {
                e.printStackTrace();
                String errMsg = "Error parsing response from PaloAltoNetworks API (3).  "
                        + "The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new ProviderException(errMsg, e);
            }
            catch (IOException e) {
                e.printStackTrace();
                String errMsg = "Error parsing response from PaloAltoNetworks (4).  "
                        + "The exception is: " + e.getMessage();
                logger.error(LOGTAG + errMsg);
                throw new ProviderException(errMsg, e);
            }
        }
    }
}
