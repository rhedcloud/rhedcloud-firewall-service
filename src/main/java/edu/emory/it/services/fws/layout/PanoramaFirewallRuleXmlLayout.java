package edu.emory.it.services.fws.layout;

import java.io.Serializable;

import org.jdom.Document;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;
import org.openeai.moa.XmlEnterpriseObject;

/**
 * Created by Tod Jackson on 12/12/2021
 * So far, it looks like the paloaltolayout manager will work for panorama too.
 * So, I'm just creating this class just in case we discover something different.
 * Otherwise, i'll just delegate all the heavy lifting to the superclass (PaloAltoFirewallRuleXmlLayout)
 */
@SuppressWarnings("serial")
public class PanoramaFirewallRuleXmlLayout extends PaloAltoFirewallRuleXmlLayout implements EnterpriseLayoutManager, Serializable {

    /**
     * Empty constructor
     */
    public PanoramaFirewallRuleXmlLayout() {

    }

    public void init(String layoutManagerName, Document layoutDoc) throws EnterpriseLayoutException {
        super.init(layoutManagerName, layoutDoc);
    }

    @Override
    public void buildObjectFromInput(Object inputObj, XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
    	super.buildObjectFromInput(inputObj, xeo);
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
    	return super.buildOutputFromObject(xeo);
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo, String appName) throws EnterpriseLayoutException {
    	return super.buildOutputFromObject(xeo, appName);
    }

}
