package edu.emory.it.services.fws;

/**
 * A release tag for the Emory Firewall Service.
 * <p>
 *
 * @author Henry Lai (hlai@surgeforward.com)
 * @version 1.0  - 6 February 2018
 */
public abstract class ReleaseTag {
    public static String space = " ";
    public static String notice = "***";
    public static String releaseName = "Emory Firewall Service";
    public static String releaseNumber = "Release 1.0";
    public static String buildNumber = "Build ####";
    public static String copyRight = "Copyright 2018 Emory University. All Rights Reserved.";

    public static String getReleaseInfo() {
        StringBuilder buf = new StringBuilder();
        buf.append(notice);
        buf.append(space);
        buf.append(releaseName);
        buf.append(space);
        buf.append(releaseNumber);
        buf.append(space);
        buf.append(buildNumber);
        buf.append(space);
        buf.append(copyRight);
        buf.append(space);
        buf.append(notice);
        return buf.toString();
    }
}
