# The RHEDcloud Firewall Service

##Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

##License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

##Description
The firewall service exposes Emory's on premises Palo Alto firewall to the ESB to allow other applications and services to query for firewall rules in place by tag. For example, this allows the VPCP web app to query the firewall for all firewall rule exceptions associated with a specific VPC.
